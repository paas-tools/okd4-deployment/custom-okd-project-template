# custom-okd-project-template

Component responsible for configuring the behavior of a cluster's user projects.

Configuration of the user project template:
- configure whether users are able to self provision projects or not. More info on [upstream doc](https://docs.okd.io/latest/applications/projects/configuring-project-creation.html#disabling-project-self-provisioning_configuring-project-creation)
- `project.config.openshift.io/cluster`: singleton resource that controls which template will be used when a user creates a project; More info on [upstream doc](https://docs.okd.io/latest/applications/projects/configuring-project-creation.html#modifying-template-for-new-projects_configuring-project-creation)
- `Template`: Openshift template that will contain all the objects/resources that should be created when a user creates a project. This resource will be the one reference by the `Project` singleton; More info on [upstream doc](https://docs.okd.io/latest/applications/projects/configuring-project-creation.html#modifying-template-for-new-projects_configuring-project-creation)

And also configuration of what is offered by OKD for users to create in projects:
- `helmchartrepositories/redhat-helm-repo` - the default source for Helm charts from Red Hat.

NB: we could also add custom CERN `HelmChartRepositories` if useful. (The difference with an Helm Operator is that
a Helm chart deployed from a HelmChartRepository only provides the initial deployment and does not manage updates like
an operator does)

This component will be deployed in each cluster specific component for all the different cluster use cases (folder `paas`, `drupal` and `webeos`) and its configuration will change accordingly.
